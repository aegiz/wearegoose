/* Goose custom code */
import { defaultModal } from './modal';

export default function () {
    const modal = defaultModal();
    $('.goose-quickSearch-cta').on('click', event => {
        event.preventDefault();
        modal.open({ pending: false });
        $(".goose-quickSearch-content").contents().appendTo('#modal');
        modal.$modal.addClass("modal--goose");
    });
}
/* */