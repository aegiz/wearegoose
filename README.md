# We Are Goose - technical test Adrien

## Instructions 

### How to see the new header?

In order to isolate as much as possible my code from the Big commerce corner stone theme, I made a separate variation of it.

There are three ways to see the new header:

1- Launch the dev server with the editor mode activated:
```sh
$ stencil start -e
```
Then, go to the theme editor URL and in the style section select "Goose".

2- Force the Goose variation when launching the dev server 
```sh
$ stencil start -e -v Goose 
```

3- In `config.json` modify the key `goose_header_type` with the value `goose_custom`.

### How to fill the custom text div (top left corner of the header)

I made the decision to have the option for a non-technical to fill this field out.
So, first, connect to the theme editor interface. In the left sidebar find the "Header and Footer" section. Finally, fillout the custom text section there.

## Best practice followed

- Always try to isolate my code in a separate file
- If not possible, (like in config.json or shema.json) prefix with "goose" to quickly find occurences (same with CSS / HTML / JS).
- Leave comments in the JS.
- Re-use as much as possible of the existing Stencil components (ex modal and collapsible).
- Put the icons in the given folder and run the grunt task to generate the corresponding sprite.

## Other consideration

- To develop the SCSS faster, I used my own custom mixin called "flex" because Stencil is limited to the 5.5.3 of Foundation. 
(Foundation 6, integrated a native support for the flew row).
